import java.util.*;

// This class represents a directed graph using adjacency 
// node representation 
public class TopologicalSort {
	
	Stack<Node> stack;
	
	//Constructor
	public TopologicalSort() {
		stack=new Stack<>();
	}
	static class Node
	{
		int data;
		boolean visited;
		List<Node> neighbours;
 
		Node(int data)
		{
			this.data=data;
			this.neighbours=new ArrayList<>();
 
		}
		//add neighbor nodes
		public void addneighbours(Node neighbourNode)
		{
			this.neighbours.add(neighbourNode);
		}
		//return nodes
		public List<Node> getNeighbours() {
			return neighbours;
		}
		//set neighbor nodes
		public void setNeighbours(List<Node> neighbours) {
			this.neighbours = neighbours;
		}
		public String toString()
		{
			return ""+data;
		}
	}
 
	 /**
     * Returns a topological order if the nodes  have a topologial order,
     * and {@code null} otherwise
     */
	public  void topologicalSort(Node node)
	{
		List<Node> neighbours=node.getNeighbours();
		for (int i = 0; i < neighbours.size(); i++) {
			Node n=neighbours.get(i);
			if(n!=null && !n.visited)
			{
				// Call the recursive helper function to store 
                // Topological Sort starting from all nodes 
                // one by one 
				topologicalSort(n);
				//Mark current node as visited
				n.visited=true;
			}
		}
		stack.push(node);
	}
    
    // Driver method 
	public static void main(String arg[])
	{
 
		TopologicalSort topological = new TopologicalSort();
		Node node_a =new Node(32);
		Node node_b =new Node(69);
		Node node_c =new Node(19);
		Node node_d =new Node(79);
		Node node_e =new Node(76);
		Node node_f =new Node(81);
		Node node_g =new Node(9);
 
		node_a.addneighbours(node_b);
		node_a.addneighbours(node_c);
		node_b.addneighbours(node_d);
		node_c.addneighbours(node_b);
		node_c.addneighbours(node_d);
		node_c.addneighbours(node_e);
		node_c.addneighbours(node_f);
		node_d.addneighbours(node_e);
		node_e.addneighbours(node_g);
		node_f.addneighbours(node_g);
		
		System.out.println("Topological Node Order:");
		topological.topologicalSort(node_a);
		
		//Stack coontent
		Stack<Node> resultStack=topological.stack;
		while (resultStack.empty()==false)
			System.out.print(resultStack.pop() + " ");
	}
}
