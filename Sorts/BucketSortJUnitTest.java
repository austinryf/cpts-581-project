import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class BucketSortJUnitTest {
	@Test
	//tests hash function with return of simple int
	public void hashTestSuccessInt() {
		BucketSort test = new BucketSort();
		int testNum = 40;
		int testBucketSize = 5;
		
		int output = test.hash(testNum, testBucketSize);
		
		assertEquals(8, output);
	}
	
	@Test
	//tests hash function with return of a rounded double into an int
	public void hashTestSuccessRoundDouble() {
		BucketSort test = new BucketSort();
		int testNum = 42;
		int testBucketSize = 4;
		
		int output = test.hash(testNum, testBucketSize);
		
		assertEquals(10, output);
	}
	
	@Test
	//tests hash function failure handling
	public void hashTestFailure() {
		BucketSort test = new BucketSort();
		int testNum = 10;
		int testBucketSize = 0;
		
		int output = test.hash(testNum, testBucketSize);
		
		assertEquals(-1, output);
	}
	
	@Test
	//tests sort algorithm on an array of 13 elements
	public void bucketSortTest_13_Elements() {
		BucketSort test = new BucketSort();
		int[] testArrayUnsorted = {34, 65, 85, 30, 47, 58, 30, 79, 27, 87, 15, 2, 17};
		int[] testArraySorted = {2, 15, 17, 27, 30, 30, 34, 47, 58, 65, 79, 85, 87};
		int testArrayLength = testArrayUnsorted.length;
		
		test.bucketSort(testArrayUnsorted, testArrayLength);
		
//		System.out.println(Arrays.toString(testArrayUnsorted));
//		System.out.println(Arrays.toString(testArraySorted));
		
		assertArrayEquals(testArraySorted, testArrayUnsorted);
	}
	
	@Test
	//tests sort algorithm on an array of 95 elements
	public void bucketSortTest_95_Elements() {
		BucketSort test = new BucketSort();
		int[] testArrayUnsorted = {31, 73, 26, 18, 13, 42, 75, 97, 82, 51, 49, 11, 70, 25, 19, 14, 78, 48, 34, 81, 41, 100, 
									3, 37, 92, 16, 9, 59, 79, 58, 32, 68, 91, 45, 61, 38, 43, 77, 76, 21, 95, 87, 10, 50, 
									63, 36, 67, 5, 52, 6, 71, 33, 85, 30, 53, 99, 22, 12, 65, 80, 46, 69, 98, 57, 35, 29, 
									86, 39, 66, 8, 24, 90, 17, 89, 44, 20, 60, 94, 74, 83, 84, 2, 1, 23, 4, 15, 7, 54, 72, 
									47, 27, 56, 88, 62, 64};
		int[] testArraySorted = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 
								25, 26, 27, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 41, 42, 43, 44, 45, 46, 47, 48, 
								49, 50, 51, 52, 53, 54, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 
								72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 94, 
								95, 97, 98, 99, 100};
		int testArrayLength = testArrayUnsorted.length;
		
		test.bucketSort(testArrayUnsorted, testArrayLength);
		
//		System.out.println(Arrays.toString(testArrayUnsorted));
//		System.out.println(Arrays.toString(testArraySorted));
		
		assertArrayEquals(testArraySorted, testArrayUnsorted);
	}
	
	@Test
	//tests sort algorithm on an array of 1 element
	public void bucketSortTest_1_Element() {
		BucketSort test = new BucketSort();
		
		int[] testArrayUnsorted = {15};
		int[] testArraySorted = {15};
		int testArrayLength = testArrayUnsorted.length;
		
		test.bucketSort(testArrayUnsorted, testArrayLength);
		
//		System.out.println(Arrays.toString(testArrayUnsorted));
//		System.out.println(Arrays.toString(testArraySorted));
		
		assertArrayEquals(testArraySorted, testArrayUnsorted);
	}
	
	@Test
	//tests sort algorithm on an array of no elements
	public void bucketSortTest_0_Elements() {
		BucketSort test = new BucketSort();
		
		int[] testArrayUnsorted = {};
		int[] testArraySorted = {};
		int testArrayLength = testArrayUnsorted.length;
		
		test.bucketSort(testArrayUnsorted, testArrayLength);
		
//		System.out.println(Arrays.toString(testArrayUnsorted));
//		System.out.println(Arrays.toString(testArraySorted));
		
		assertArrayEquals(testArraySorted, testArrayUnsorted);
	}
	
	@Test
	//tests the sorting of individual buckets in a list of buckets
	public void sortBucketsTest() {
		BucketSort test = new BucketSort();
		
		List<Integer>[] testListUnsorted = (List<Integer>[]) new List[2];
		List<Integer>[] testListSorted = (List<Integer>[]) new List[2];
		List<Integer>[] testListShouldSort = (List<Integer>[]) new List[2];
		
		List<Integer> inside1 = new ArrayList<Integer>();
		inside1.add(4); inside1.add(1); inside1.add(7);
		List<Integer> inside2 = new ArrayList<Integer>();
		inside2.add(3); inside2.add(2);
		
		testListUnsorted[1] = inside1;
		testListUnsorted[0] = inside2;

		testListSorted[0] = inside2;
		testListSorted[1] = inside1;

		
		testListShouldSort = test.sortBuckets(testListUnsorted);
		
		assertThat(testListShouldSort, is(testListSorted));
	}
}
