import static org.junit.Assert.*;
import java.util.Arrays;
import org.junit.Test;

class CycleSortJUnitTest {
	@Test
	public void testCycleSort()
	{
		int arr[] = {34, 65, 85, 30, 47, 58, 30, 79 }
		int n = arr.length;		
		cycleSort(arr,n);		
		int test = 30;
		int output = arr[0];
		assertEquals(test, output);
	}
	@Test 
	public void testArrayLength()
	{
		int arr[] = {34, 56};
		int n = arr.length;
		assertEquals(1, n);
		
	}
	@Test 
	public void zeroElementsCycleSort()
	{	
		int testUnsortedArray = {};
		int testSortedArray = {};
		int testArrayLength = testUnsortedArray.length;
		
		assertArrayEquals(testSortedArray, testUnsortedArray);	
	}
	@Test 
	public void oneElementsCycleSort()
	{	
		int testUnsortedArray = {67};
		int testSortedArray = {67};
				
		assertArrayEquals(testSortedArray, testUnsortedArray);	
	}
 }