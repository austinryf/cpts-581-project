import static org.junit.Assert.*;
import java.util.Arrays;
import org.junit.Test;

import TopologicalSort.Node;

public class TopologicalSortJUnitTest {
private static final String Node = null;

//	@Test
	public void topologicaltestSuccess() {
		TopologicalSort test = new TopologicalSort();
		Node node_a =new Node(32);
		Node node_b =new Node(69);
		Node node_c =new Node(19);
		Node node_d =new Node(79);
		Node node_e =new Node(76);
		Node node_f =new Node(81);
		Node node_g =new Node(9);

		node_a.addneighbours(node_b);
		node_a.addneighbours(node_c);
		node_b.addneighbours(node_d);
		node_c.addneighbours(node_b);
		node_c.addneighbours(node_d);
		node_c.addneighbours(node_e);
		node_c.addneighbours(node_f);
		node_d.addneighbours(node_e);
		node_e.addneighbours(node_g);
		node_f.addneighbours(node_g);
	
		
		int output = test.topologicalSort(node_a);
		int x[] = {32,19,76,81};
		assertEquals(x ,output )
	}
	
	@Test	
	public void topologicaltestFailure() {
		TopologicalSort test = new TopologicalSort();
		Node node_a =new Node(32);
		Node node_b =new Node(69);
		Node node_c =new Node(19);
		Node node_d =new Node(81);
		Node node_e =new Node(76);
		Node node_f =new Node(81);
		Node node_g =new Node(9);

		node_a.addneighbours(node_b);
		node_d.addneighbours(node_e);
		node_e.addneighbours(node_g);
		node_f.addneighbours(node_g);
	
		
		int output = test.topologicalSort(node_a);
		int x[] = {65,13,57,76};
		assertEquals(x ,output )
	}
	
}