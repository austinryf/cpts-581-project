import java.util.concurrent.CountDownLatch;

 
public class SleepSort {
	
	public static void sleepSort(int[] nums) {
		System.out.print("here");
		System.out.print(nums.length);
		
		threadRun(nums);
		
		System.out.print("here");
	}
	
	//moved to its own function to improve testability
	private static void threadRun(int[] nums) {
		final CountDownLatch finish = new CountDownLatch(nums.length);
		// all numbers are given their own "process" and are "waken up" in order from least to greatest
		// when they are woken up they are printed and there for create a printed sorted list
		for (final int num : nums) {
					
					new Thread(new Runnable() {
						
						public void run() {
							
							finish.countDown();
							
							try {
								finish.await();
								//sleeps the thread to demonstrate the computer "sleeping before waking up the process"
								Thread.sleep(num * 200);
								System.out.print(num+",");
							} 
							
							catch (InterruptedException e) {
								e.printStackTrace();
							}
						}			
					}).start();
		}
	}
	
	public static void unitTestMain(int[] nums) {		
		sleepSort(nums);
	}
	
	public static void main(String[] args) {
		int[] nums = {55, 85, 49, 60, 94, 97, 8, 12, 90, 92, 1, 9, 6, 26, 29, 99, 37, 33, 74, 19};
		
		for (int i = 0; i < args.length; i++) {
			nums[i] = Integer.parseInt(args[i]);
		}
		
		sleepSort(nums);
	}
}