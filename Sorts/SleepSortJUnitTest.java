import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;


public class SleepSortJUnitTest {
	
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
	private final PrintStream originalOut = System.out;
	private final PrintStream originalErr = System.err;

	@Before
	public void setUpStreams() {
	    System.setOut(new PrintStream(outContent));
	    System.setErr(new PrintStream(errContent));
	}

	@After
	public void restoreStreams() {
	    System.setOut(originalOut);
	    System.setErr(originalErr);
	}
	
	//Unit testing with Thread.sleep proved very difficult so I tested to make sure that the program was making it inside of
	//the sleepSort function, that the correct array was being passed in, and that it made it to the end of the function
	//without crashing
	@Test
	public void mainUnitTest1() {
		SleepSort test = new SleepSort();
		int[] nums = {4, 2, 5, 3, 1};

		test.unitTestMain(nums);
	    assertEquals("here5here", outContent.toString());
	}
	
	@Test
	public void mainUnitTest2() {
		SleepSort test = new SleepSort();
		int[] nums = {37, 93, 39, 73, 58, 48, 45, 28, 17, 2, 84};

		test.unitTestMain(nums);
	    assertEquals("here11here", outContent.toString());
	}
	
	@Test
	public void mainUnitTestOneLen() {
		SleepSort test = new SleepSort();
		int[] nums = {1};

		test.unitTestMain(nums);
	    assertEquals("here1here", outContent.toString());
	}
	
	@Test
	public void mainUnitTestZerolen() {
		SleepSort test = new SleepSort();
		int[] nums = {};

		test.unitTestMain(nums);
	    assertEquals("here0here", outContent.toString());
	}
	
	@Test
	public void mainUnitTestOneHunnidlen() {
		SleepSort test = new SleepSort();
		int[] nums = {87, 75, 50, 63, 11, 28, 37, 64, 76, 82, 20, 96, 2, 18, 
				41, 35, 47, 9, 10, 58, 78, 93, 72, 17, 22, 36, 100, 81, 51, 
				67, 29, 98, 38, 3, 49, 32, 88, 55, 99, 74, 83, 5, 85, 16, 90, 
				60, 34, 45, 33, 52, 31, 89, 68, 12, 73, 91, 8, 27, 70, 56, 69, 
				79, 46, 62, 77, 97, 57, 19, 39, 61, 84, 24, 71, 65, 7, 40, 54, 
				53, 25, 1, 59, 23, 26, 13, 86, 42, 30, 66, 6, 92, 48, 95, 15, 
				94, 4, 14, 21, 43, 80, 44};

		test.unitTestMain(nums);
	    assertEquals("here100here", outContent.toString());
	}
	
}