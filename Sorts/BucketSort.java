import java.util.ArrayList; 
import java.util.Arrays; 
import java.util.Collections;
import java.util.LinkedList;
import java.util.List; 


/* * Java Program sort an integer array using radix sort algorithm. 
 * * input: [80, 50, 30, 10, 90, 60, 0, 70, 40, 20, 50] 
 * * output: [0, 10, 20, 30, 40, 50, 50, 60, 70, 80, 90] 
 * * * Time Complexity of Solution: 
 * * Best Case O(n); Average Case O(n); Worst Case O(n^2). 
 * * */ 

public class BucketSort {
	
	public static void main(String[] args) {
        int[] unsortedArray1 = {10, 13, 3, 94, 35, 38, 6, 45, 27, 29, 63, 86};
        System.out.println("Unsorted Array - " + Arrays.toString(unsortedArray1));
        bucketSort(unsortedArray1, 10);
        System.out.println("Sorted Array - " + Arrays.toString(unsortedArray1));
        System.out.println("");
        
        int[] unsortedArray2 = {89, 73, 23, 49, 12, 37, 5, 96, 2, 19, 17, 35, 14, 54, 87, 47, 62, 3, 12, 63, 67, 97};
        System.out.println("Unsorted Array - " + Arrays.toString(unsortedArray2));
        bucketSort(unsortedArray2, 20);
        System.out.println("Sorted Array - " + Arrays.toString(unsortedArray2));
    }
	
	//moved to its own function to improve testability
	//sorts each bucket in the list of buckets
	//these lists of buckets are in the form of [[3, 1], [6, 2, 9], [8, 5], [4], [7]]
	public static List<Integer>[] sortBuckets(List<Integer>[] buckets) {
		for(List<Integer> bucket : buckets){
            Collections.sort(bucket);
        }
		
		return buckets;
	}
    
    static void bucketSort(int[] arr, int bucketSize){
        // Create bucket array for storing lists
        List<Integer>[] buckets = new List[bucketSize];
        // Linked list with each bucket array index
        // as there may be hash collision 
        if(bucketSize <= 1) {
    		System.out.println("Something went wrong (Array must be longer than 1 element), this array contains " + arr.length + " element(s)");
    		return;
        }
        //puts each individual bucket into its own list, for the sorting algorithm
        else {
            for(int i = 0; i < bucketSize; i++){
                buckets[i] = new LinkedList<>();
            }
        }
        
        // calculate hash and assign elements to proper bucket
        for(int num : arr){
        	if(hash(num,bucketSize) != -1) {
        		buckets[hash(num, bucketSize)].add(num);
        	}
        	else {
        		System.out.println("Something went wrong (Bucket Size cannot be less than 1)");
        		return;
        	}
        }    
        
        sortBuckets(buckets);
 
        int index = 0;
        // Merge buckets to get sorted array
        for(List<Integer> bucket : buckets){
            
            for(int num : bucket){
                arr[index++] = num;
            }
        }
    }
    
    // hash function used for element distribution 
    static int hash(int num, int bucketSize){
    	if(bucketSize != 0) {
    		return num/bucketSize;
    	}
    	else {
    		return -1;
    	}
    }
} 
