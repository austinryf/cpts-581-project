import static org.junit.Assert.*;
import java.util.Arrays;
import org.junit.Test;

class DepthFirstSearchJUnitTest {
	@Test
	public void testEmptyDepthFirstSearchSort()
	{
		DepthFirstSearch test = new DepthFirstSearch(5);
		DepthFirstSearch testOutput = new DepthFirstSearch(5);
				
		assertArrayEquals(testOutput, test);
		
	}
	@Test
	public void testDepthFirstSearchSort()
	{
		DepthFirstSearch test = new DepthFirstSearch(5);
		DepthFirstSearch testOutput = new DepthFirstSearch(5);
		test.addEdge(0,1);
		test.addEdge(3,4);
		test.DFS();
		
		testOutput(2, 3);
		testOutput.DFS();	
		
		assertArrayEquals(testOutput, test);
		
	}
	@Test
	public void testFailedDepthFirstSearchSort()
	{
		DepthFirstSearch test = new DepthFirstSearch(5);
		DepthFirstSearch testOutput = new DepthFirstSearch(5);
		test.addEdge(0,1);
		test.addEdge(0,4);
		test.DFS();
		
		assertArrayEquals(testOutput, test);
		
	}

}