import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

import static java.lang.String.format;

/**
 *
 *
 *
 * Binary search is one of the most popular algorithms
 * The algorithm finds the position of a target value within a sorted array
 *
 * Worst-case performance	O(log n)
 * Best-case performance	O(1)
 * Average performance	O(log n)
 * Worst-case space complexity	O(1)
 *
 *
 * @author Varun Upadhyay (https://github.com/varunu28)
 * @author Podshivalov Nikita (https://github.com/nikitap492)
 *
 * @see SearchAlgorithm
 * @see IterativeBinarySearch
 *
 */

public class BinarySearch implements SearchAlgorithm {

    /**
     *
     * @param array is an array where the element should be found
     * @param key is an element which should be found
     * @param <T> is any comparable type
     * @return index of the element
     */
    @Override
    public  <T extends Comparable<T>> int find(T[] array, T key) {
        return search(array, key, 0, array.length);
    }
    
	//moved to its own function to improve testability
    //this function will find the median (value in the middle) of the current array and
    // the position of the key (searched value) relative to that median
    public static <T extends Comparable<T>> int[] medianCompare(int left, int right, T key, T array[]) {
    	//median is the first value, position of key is the second value
    	int mediancomp[] = {0, 0};
    	
    	//the >>> bitwise operator will find the rounded center of a list of size left + right
    	mediancomp[0] = (left + right) >>> 1;
    	//find the position of the key in relation to the median (-1 is left, 1 is right, and 0 is key found)
    	mediancomp[1] = key.compareTo(array[mediancomp[0]]);
    	
    	return mediancomp;
    }
    
    
    /**
     * This method implements the Generic Binary Search
     *
     * @param array The array to make the binary search
     * @param key The number you are looking for
     * @param left The lower bound
     * @param right The  upper bound
     * @return the location of the key
     **/
    public static <T extends Comparable<T>> int search(T array[], T key, int left, int right){
        if (right < left) return -1; // this means that the key not found
        
        //returns [median, position of key relative to median]
        int[] medcomp = medianCompare(left, right, key, array);
        
        //if key is found return key
        if (medcomp[1] == 0) {
            return medcomp[0];
        } else if (medcomp[1] < 0) { //if key is to the left run search again with a truncated list left of the median
            return search(array, key, left, medcomp[0] - 1);
        } else { //if key is to the right run search again with a truncated list right of the median
            return search(array, key, medcomp[0] + 1, right);
        }
    }
    
	//moved to its own function to improve testability
    //simply prints information about the search algorithm
    public static void printFound(int size, int shouldBeFound, Integer[] integers, int atIndex) {
        System.out.println(format(
                "Should be found: %d. Found %d at index %d. An array length %d",
                shouldBeFound, integers[atIndex], atIndex, size
            ));

            int toCheck = Arrays.binarySearch(integers, shouldBeFound);
            System.out.println(format("Found by system method at an index: %d. Is equal: %b", toCheck, toCheck == atIndex));
    }
    

    // Driver Program
    public static void main(String[] args) {
        // Just generate data
        Random r = ThreadLocalRandom.current();

        int size = 100;
        //int size = 1;
        int maxElement = 100000;
        
        Integer[] integers = IntStream.generate(() -> r.nextInt(maxElement)).limit(size).sorted().boxed().toArray(Integer[]::new);


        // The element that should be found
        //changed [r.nextInt(size)-1] to [r.nextInt(size)] as the program would crash if the array was only 1 element long 
        int shouldBeFound = integers[r.nextInt(size)];

        BinarySearch search = new BinarySearch();
        int atIndex = search.find(integers, shouldBeFound);

        printFound(size, shouldBeFound, integers, atIndex);
    }
    
    //a unit testable version of main
    public static int binarySearch(Integer[] integers, int shouldBeFound) {

        BinarySearch search = new BinarySearch();
        int atIndex = search.find(integers, shouldBeFound);
        
        return atIndex;
    }
}
