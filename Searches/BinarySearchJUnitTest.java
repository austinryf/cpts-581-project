import static org.junit.Assert.*;
import java.util.Arrays;
import org.junit.Test;



public class BinarySearchJUnitTest {
	@Test
	//tests an array of odd length
	public void searchTestOddLen() {
		BinarySearch test = new BinarySearch();
		Integer[] testArray = {1, 2, 3, 4, 5};
		int testKey = 2;
		int testLeft = 0;
		int testRight = testArray.length;
		
		int output = test.search(testArray, testKey, testLeft, testRight);
		
		assertEquals(1, output);
	}
	
	@Test
	//tests an array of even length
	public void searchTestEvenLen() {
		BinarySearch test = new BinarySearch();
		Integer[] testArray = {2, 4, 6, 8};
		int testKey = 8;
		int testLeft = 0;
		int testRight = testArray.length;
		
		int output = test.search(testArray, testKey, testLeft, testRight);
		
		assertEquals(3, output);
	}
	
	@Test
	//tests an array of length one
	public void searchTestOneLen() {
		BinarySearch test = new BinarySearch();
		Integer[] testArray = {64};
		int testKey = 64;
		int testLeft = 0;
		int testRight = testArray.length;
		
		int output = test.search(testArray, testKey, testLeft, testRight);
		
//		System.out.println(output);
		
		assertEquals(0, output);
	}
	
	@Test
	//tests an array where the key is not present
	public void searchTestNegOne() {
		BinarySearch test = new BinarySearch();
		Integer[] testArray = {12, 54, 87, 57, 123, 89, 67, 56, 40};
		int testKey = 100;
		int testLeft = 0;
		int testRight = testArray.length;
		//should return -1 for 'failure'
		int output = test.search(testArray, testKey, testLeft, testRight);
		
		assertEquals(-1, output);
	}
	
	@Test
	//tests key position in random array
	public void binarySearchTest1() {
		BinarySearch test = new BinarySearch();
		Integer[] testArray = {12, 54, 87, 57, 123, 89, 67, 56, 40};
		int testShouldBeFound = 123;
		
		
		int output = test.binarySearch(testArray, testShouldBeFound);

		//System.out.println(output);
		
		assertEquals(4, output);
	}
	
	@Test
	//tests key position in random longer array
	public void binarySearchTest2() {
		BinarySearch test = new BinarySearch();
		Integer[] testArray = {18, 10, 42, 79, 25, 84, 32, 59, 50, 64, 71, 63, 82, 23, 70, 49, 26, 5, 83, 68};
		int testShouldBeFound = 64;
		
		
		int output = test.binarySearch(testArray, testShouldBeFound);

//		System.out.println(testArray[9]);
//		System.out.println(output);
//		System.out.println(testArray.length);
		
		assertEquals(9, output);
	}
	
	@Test
	//tests key position in array of length one
	public void binarySearchTest3() {
		BinarySearch test = new BinarySearch();
		Integer[] testArray = {15};
		int testShouldBeFound = 15;
		
		
		int output = test.binarySearch(testArray, testShouldBeFound);

//		System.out.println(testArray[9]);
//		System.out.println(output);
//		System.out.println(testArray.length);
		
		assertEquals(0, output);
	}
	
	@Test
	//tests key position left of median
	public void medianCompareTestBelow() {
		BinarySearch test = new BinarySearch();
		Integer[] testArray = {1, 2, 3, 4, 5, 6};
		int testKey = 2;
		int testLeft = 0;
		int testRight = testArray.length;
		
		int[] output = test.medianCompare(testLeft, testRight, testKey, testArray);
		
		int[] arr = {3, -1};
		assertArrayEquals(arr, output);
	}
	
	@Test
	//tests key position right of median
	public void medianCompareTestAbove() {
		BinarySearch test = new BinarySearch();
		Integer[] testArray = {1, 2, 3, 4, 5, 6};
		int testKey = 5;
		int testLeft = 0;
		int testRight = testArray.length;
		
		int[] output = test.medianCompare(testLeft, testRight, testKey, testArray);
		
		int[] arr = {3, 1};
		assertArrayEquals(arr, output);
	}
	
	@Test
	//tests key position on median
	public void medianCompareTestOn() {
		BinarySearch test = new BinarySearch();
		Integer[] testArray = {1, 2, 3, 4, 5, 6, 7};
		int testKey = 4;
		int testLeft = 0;
		int testRight = testArray.length;
		
		int[] output = test.medianCompare(testLeft, testRight, testKey, testArray);
		
		int[] arr = {3, 0};
		assertArrayEquals(arr, output);
	}
}