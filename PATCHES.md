## Patch 0, Version 1.0
  * Cloned repo to begin work

## Patch 1, Version 1.1
  * Added BucketSort feature to the Sorts directory (cpts-581-project/Sorts/BucketSort.java)

  * Added unit tests for BucketSort feature (cpts-581-project/Sorts/BucketSortJUnitTest.java)
    * Unit Test hashTestSuccessInt()
    * Unit Test hashTestSuccessRoundDouble()
    * Unit Test hashTestFailure()
    * Unit Test bucketSortTest_13_Elements()
    * Unit Test bucketSortTest_95_Elements()
    * Unit Test bucketSortTest_1_Element()
    * Unit Test bucketSortTest_0_Elements()

## Patch 2, Version 1.2
  * Fixed a bug in BinarySearch in the Searches directory (cpts-581-project/Searches/BinarySearch.java)
    * Bug was on line 82 and would cause the program to crash if the array was of length 1 or less

  * Added unit tests for BinarySearch (cpts-581-project/Searches/BinarySearchJUnitTest.java)
    * Unit Test searchTestOddLen()
    * Unit Test searchTestEvenLen()
    * Unit Test searchTestOneLen()
    * Unit Test searchTestNegOne()
    * Unit Test binarySearchTest2()
    * Unit Test binarySearchTest3()

## Patch 3, Version 1.3
  * Added SleepSort feature to the Sorts directory (cpts-581-project/Sorts/SleepSort.java)

  * Added unit tests for SleepSort feature (cpts-581-project/Sorts/SleepSortJUnitTest.java)
    * Unit Test mainUnitTest1()
    * Unit Test mainUnitTest2()
    * Unit Test mainUnitTestOneLen()
    * Unit Test mainUnitTestZerolen()
    * Unit Test mainUnitTestOneHunnidlen()

## Patch 4, Version 1.3.5
  * Updated DIRETORY.md to redirect BucketSort, BinarySearch, and SleepSort to our repo's code
  * Added PATCHES.md

## Patch 5, Version 1.4
  * Updated Depth First Search and added accompanying unit tests
  * Updated Cycle Sort and added accompanying unit tests
  * Updated Topological Sort and added accompanying unit tests

## Patch 6, Version 1.5
  * Improved testability in BucketSort, Binary Search, and Sleep Sort by moving code in lengthy functions into their own functions
  * Added more unit tests for those classes

## Patch 7, Version 1.6
  * Improved readability and code comprehension of code by adding comments explaining all functions in BucketSort, Binary Search, and Sleep Sort
  * Improved readability and code comprehension of tests by adding comments explaining all test cases in BucketSortJUnitTest, Binary SearchJUnitTest, and Sleep SortJUnitTest

## Patch 8, Version 1.7
  * Improved readability and code comprehension of code by adding comments explaining all functions in topological sort


## Patch 9, Version 1.8
 * Performed changeability by restructuring the code to improve and simplify  how it can be tested in cycle sort algorithm.
 * Improved readability and code comprehension of tests by adding comments

